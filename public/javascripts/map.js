var mymap = L.map('main_map').setView([2.92382, -75.28970], 13);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoicGlwZW1hcnRpbiIsImEiOiJja2J2NHhxNTYwMmxoMzBvYnJ0c21kOXFsIn0.9wOGq6yOwzyaN_BUSgSyGA'
}).addTo(mymap);

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result){
    console.log(result);
    result.bicicletas.forEach(function(bici){
      L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
    });

  }
})
