var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
var base_url="http://localhost:5000/api/bicicleta";

describe('Bicicleta API', ()=>{

  beforeEach(function(done){
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, {useNewUrlParser: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function(){
      console.log('we are connected to test database');
      done();
    });
  });

  afterEach(function(done){
    Bicicleta.deleteMany({}, function(err, success){
      if (err) console.log(err);
      done();
    });
  });

  describe('Get BICICLETAS /', ()=>{
    it('Status 200',(done)=>{
      request.get(base_url, function(error, response, body){
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicleta.length).toBe(0);
        done();
      });
    });
  });

  describe('POST BICICLETAS /create', ()=>{
    it('STATUS 200', (done)=>{
      var headers = {'content-type':'application/json'};
      var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": 2.923609, "lng": -75.287728}';
      request.post({
        headers: headers,
        url: base_url + '/createe',
        body: aBici
      }, function(error, response, body){
        expect(response.statusCode).toBe(200);
        var bici = JSON.parse(body).bicicleta;
        console.log(bici);
        expect(bici.color).toBe("rojo");
        expect(bici.ubicacion[0]).toBe(2.923609);
        expect(bici.ubicacion[1]).toBe(-75.287728);
        done();
      });
    });
  });



});
